<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

use Symfony\Component\HttpFoundation\RequestStack;

use App\Entity\PermissionTypes;

class PrivateDataVoter extends Voter
{
    // const CAN_VIEW_OWN_PROFILE = 'CVOP';
    // const CAN_VIEW_USERS = 'CVU';
    // const CAN_ADD_USERS = 'CAU';
    // const CAN_EDIT_USERS = 'CEU';
    // const CAN_DELETE_USERS = 'CDU';
    public function __construct(RequestStack $requestStack)
    {
        $this->session = $requestStack->getSession()->all();
    }

    protected function supports(string $attribute, $subject): bool
    {
        $authorize = false;

        if ( in_array($attribute, [PermissionTypes::CAN_VIEW_OWN_PROFILE]) ) {
            $authorize = true;
        }

        return $authorize;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $tokenInterface): bool
    {
        $permissions = $this->session['permissions'];
        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case PermissionTypes::CAN_VIEW_OWN_PROFILE:
                //  user can View profile
                if ( in_array($attribute, $permissions) ) {
                    return true;
                }
                return false;
        }

        return false;
    }
}
