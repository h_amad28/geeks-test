<?php

namespace App\Repository;

use App\Entity\UserRoles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Roles;
use App\Entity\RolePermissions;
use App\Entity\Permissions;
use App\Entity\PermissionTypes;

/**
 * @method UserRoles|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserRoles|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserRoles[]    findAll()
 * @method UserRoles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRolesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserRoles::class);
    }

    // /**
    //  * @return UserRoles[] Returns an array of UserRoles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserRoles
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * 
     *  user Role figuring out granted permission
     *      different roles has different permissions
     *      permissions has short_codes like CAU (can add user) etc
     * */
    public function getRolesByUserId(int $userId): ?array
    {
        $result = [ PermissionTypes::CAN_VIEW_OWN_PROFILE ];// Default own profile permission
        $result = $this->createQueryBuilder('ur')
            ->innerJoin(Roles::class, 'r', 'with', 'r.id = ur.role_id')
            ->innerJoin(RolePermissions::class,'rp', 'with', 'r.id = rp.role_id')
            ->innerJoin(Permissions::class,'p', 'with', 'rp.permission_id = p.id')
            ->where('ur.user_id = :userId')
            ->setParameter('userId', $userId)
            ->select('p.short_codes')
            ->getQuery()
            ->getSingleColumnResult();
        return $result;
    }
}
