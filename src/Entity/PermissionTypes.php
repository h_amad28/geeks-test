<?php
namespace App\Entity;

class PermissionTypes{
	
	const CAN_VIEW_OWN_PROFILE = 'CVOP';
    const CAN_VIEW_USERS = 'CVU';
    const CAN_ADD_USERS = 'CAU';
    const CAN_EDIT_USERS = 'CEU';
    const CAN_DELETE_USERS = 'CDU';
}