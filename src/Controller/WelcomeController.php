<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

use App\Entity\PermissionTypes;


/**
 * 
 */
class WelcomeController extends AbstractController
{

	public function __construct()
	{
	}

	function index(){
		$this->denyAccessUnlessGranted( PermissionTypes::CAN_VIEW_OWN_PROFILE );// check for view access
		return $this->render('welcome.html.twig');
	}

}