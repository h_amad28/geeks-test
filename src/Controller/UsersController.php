<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Users;
use App\Repository\UsersRepository;
use App\Repository\UserRolesRepository;

/**
 * 
 */
class UsersController extends AbstractController
{
    private $entityManager;
    private $passwordHasher;
    private $userRepo;
    private $userRolesRepo;
    private $requestStack;
    
    function __construct(UserPasswordHasherInterface $passwordHasher, UsersRepository $userRepo, UserRolesRepository $userRolesRepo, RequestStack $requestStack)
    {
        $this->passwordHasher = $passwordHasher;
        $this->requestStack = $requestStack;
        $this->userRepo = $userRepo;
        $this->userRolesRepo = $userRolesRepo;
    }

    function index(){
        return $this->render('sign_in.html.twig', ['number' => "testing"]);
    }

    function login(Request $request, UserPasswordHasherInterface $passwordHasher){
        $method = $request->getMethod();

        if (strcmp(strtolower($method), 'post') === 0 && !empty($request->request->all())) {
            $user = $this->userRepo->findOneBy(['email' => $request->request->get('email')]);

            if ($user && $this->passwordHasher->isPasswordValid($user, $request->request->get('password'))) {
                $this->initiateUserSession($user);
                return $this->redirectToRoute('welcome');
            }else{
                $this->addFlash(
                    'warning',
                    'Invalid Username AND Password'
                );
            }
        }
        return $this->render('sign_in.html.twig');
    }

    public function signUp(Request $request, UserPasswordHasherInterface $passwordHasher)
    {
        $method = $request->getMethod();

        if (strcmp(strtolower($method), 'post') === 0 && !empty($request->request->all())) {
            return $this->registerUser($request->request->all(), $passwordHasher);
        }

        return $this->render('sign_up.html.twig');
    }

    private function registerUser(array $userData, $passwordHasher)
    {
        $this->entityManager = $this->getDoctrine()->getManager();
        
        $user = new Users();
        $user->setEmail($userData['email']);
        $encryptedPassword = $passwordHasher->hashPassword(
            $user,
            $userData['password']
        );

        $user->setFirstName($userData['first_name']);
        $user->setLastName($userData['last_name']);
        $user->setPassword($encryptedPassword);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        if ($user->getId()) {
            $this->addFlash(
                'success',
                'You have signed up successfully'
            );
            return $this->redirectToRoute('login');
        }
        return ;
    }

    private function initiateUserSession(Users $user)
    {
        $permissions = $this->userRolesRepo->getRolesByUserId( $user->getId() );
        $session = $this->requestStack->getSession();
        $session->start();
        $session->set('id', $user->getId());
        $session->set('name', $user->getFullName());
        $session->set('permissions', $permissions);
    }
}